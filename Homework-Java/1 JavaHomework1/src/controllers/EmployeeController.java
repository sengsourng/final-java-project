package controllers;

import enities.EmployeeManager;

import java.util.Scanner;

public class EmployeeController {
    static Scanner scanner = new Scanner(System.in);

    public static void AppController() {
        int work = 0;
        do {
            System.out.println("choose 1 for Add New Employee:");
            System.out.println("choose 2 for  Employee List:");
            System.out.println("choose 3 for Find Employee:");
            System.out.println("choose 4 for Update Empoyee:");
            System.out.println("choose 5 for Remove Employee:");
            System.out.println("choose 6 or other key for exit.");
            System.out.print("choose one option to operation.");
            work = scanner.nextInt();
            switch (work) {
                case 1:
                    EmployeeManager.getInstance().addEmployee();
                    break;
                case 2:
                    EmployeeManager.getInstance().viewAllEmployee();
                    break;
                case 3:
                    EmployeeManager.getInstance().viewOneEmployee();
                    break;
                case 4:
                    EmployeeManager.getInstance().updateEmployee();
                    break;
                case 5:
                    EmployeeManager.getInstance().removeEmployee();
                    break;
                default:
                    break;
            }
        } while (work < 6);
    }

}
