package services;

import constants.EmployeeConstant;
import database.MySqlConnection;
import database.SqliteConnection;

import java.sql.SQLException;

public class DatabaseService {
    private static SqliteConnection sqliteConnection = null;
    private static MySqlConnection mysqlConnection = null;

    public static SqliteConnection connection() throws SQLException {
        if (sqliteConnection == null) {
            sqliteConnection = new SqliteConnection(EmployeeConstant.URL);
        } else {
            sqliteConnection = null;
            sqliteConnection = new SqliteConnection(EmployeeConstant.URL);
        }
        return sqliteConnection;
    }

    public static MySqlConnection mysqlInstance() throws SQLException {
        if (mysqlConnection == null) {
            mysqlConnection = new MySqlConnection(EmployeeConstant.MYSQL_URL, EmployeeConstant.USERNAME, EmployeeConstant.PASSWORD);
        } else {
            mysqlConnection = null;
            mysqlConnection = new MySqlConnection(EmployeeConstant.MYSQL_URL, EmployeeConstant.USERNAME, EmployeeConstant.PASSWORD);
        }
        return mysqlConnection;
    }
}
