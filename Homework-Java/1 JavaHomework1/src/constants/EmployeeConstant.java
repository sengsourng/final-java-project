package constants;

public class EmployeeConstant {
    public final static String INSERT_EMPLOYEE = "INSERT INTO employees(first_name,last_name,gender,email,type,salary,address,rate,hours) VALUES(?,?,?,?,?,?,?,?,?)";
    public final static String UPDATE_EMPLOYEE = "UPDATE employees SET first_name=?,last_name=?,gender=?,email=?,type=?,salary=?,address=?,rate=?,hours=? WHERE emp_id=?";
    public final static String REMOVE_EMPLOYEE = "DELETE FROM employees WHERE emp_id=?";
    public final static String SELECT_ONE_EMPLOYEE = "SELECT * FROM employees WHERE emp_id=?";
    public final static String SELECT_ALL_EMPLOYEE = "SELECT * FROM employees";
    public final static String URL = "jdbc:sqlite:C:/sqlite/db/chinook.db";
    public final static String USERNAME = "root";
    public final static String PASSWORD = "";
    public final static String MYSQL_URL = "jdbc:mysql://localhost/chinook";
}
