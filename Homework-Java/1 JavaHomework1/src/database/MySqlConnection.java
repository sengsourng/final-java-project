package database;

import java.sql.SQLException;

public class MySqlConnection extends SqlClient {

    public MySqlConnection(String url) throws SQLException {
        super(url);
    }

    public MySqlConnection(String url, String username, String passwprd) throws SQLException {
        super(url, username, passwprd);
    }
}
