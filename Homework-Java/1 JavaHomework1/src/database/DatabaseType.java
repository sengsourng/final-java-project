package database;

public enum DatabaseType {
    Sqlite,
    MylSql,
    Oracle,
    SqlServer
}
