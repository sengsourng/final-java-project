package database;

import java.sql.ResultSet;

public interface IDatabase {
    boolean insert(String sql, Object... data);

    boolean update(String sql, Object... data);

    boolean delete(String sql, Object... conditions);

    int deleteMany(String sql, Object... conditions);

    String[] find(String sql, Object... conditions);

    String[][] selectManyWithColumnName(String sql, Object... conditions);

    String[][] selectMany(String sql, Object... conditions);

    String[][] selectOneWithColumnName(String sql, Object... conditions);

    ResultSet single(String sql, Object... conditions);

    ResultSet many(String sql, Object... condition);

    boolean isExist(String sql, Object... condition);
}
