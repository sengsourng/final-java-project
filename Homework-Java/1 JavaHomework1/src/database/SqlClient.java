package database;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SqlClient implements IDatabase {
    private Connection connection = null;

    public SqlClient() {
    }

    public SqlClient(String url) throws SQLException {
        this.createConnection(url);
    }

    public SqlClient(String url, String username, String password) throws SQLException {
        this.createConnection(url, username, password);
    }

    private void createConnection(String url, String username, String password) {
        try {
            this.connection = DriverManager.getConnection(url, username, password);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    private void createConnection(String url) throws SQLException {
        try {
            this.connection = DriverManager.getConnection(url);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

    }

    @Override
    public boolean insert(String sql, Object... data) {
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            int index = 1;
            for (Object o : data) {
                ps.setObject(index, o);
                index++;
            }
            return ps.executeUpdate() > 0;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            if (this.connection != null) {
                try {
                    this.connection.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    @Override
    public boolean update(String sql, Object... data) {
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            int index = 1;
            for (Object o : data) {
                ps.setObject(index, o);
                index++;
            }
            return ps.executeUpdate() > 0;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            if (this.connection != null) {
                try {
                    this.connection.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    @Override
    public boolean delete(String sql, Object... conditions) {
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            int index = 1;
            for (Object o : conditions) {
                ps.setObject(index, o);
                index++;
            }
            return ps.executeUpdate() > 0;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            if (this.connection != null) {
                try {
                    this.connection.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    @Override
    public int deleteMany(String sql, Object... conditions) {
        try {
            int deletedRow = 0;
            int index = 1;
            for (Object o : conditions) {
                PreparedStatement ps = connection.prepareStatement(sql);
                ps.setObject(index, o);
                index++;
                if (ps.executeUpdate() <= 0) continue;
                deletedRow++;
            }
            return deletedRow;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            if (this.connection != null) {
                try {
                    this.connection.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        return 0;
    }

    @Override
    public String[] find(String sql, Object... conditions) {
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ArrayList<String> value = new ArrayList<String>();
            int index = 1;
            if (conditions.length > 0 || conditions != null) {
                for (Object o : conditions) {
                    ps.setObject(index, o);
                    index++;
                }
            }
            ResultSet rs = ps.executeQuery();
            int columns = rs.getMetaData().getColumnCount();
            if (rs.next()) {
                for (int i = 1; i <= columns; i++) {
                    value.add(rs.getString(i));
                }
            }
            ;
            rs.close();
            ps.close();
            return SqlClient.convertToArray(value);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            if (this.connection != null) {
                try {
                    this.connection.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    @Override
    public String[][] selectManyWithColumnName(String sql, Object... conditions) {
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            List<ArrayList<String>> data = new ArrayList<ArrayList<String>>();
            ArrayList<String> value = new ArrayList<String>();
            int index = 1;
            if (conditions.length > 0 || conditions != null) {
                for (Object o : conditions) {
                    ps.setObject(index, o);
                    index++;
                }
            }
            ResultSet rs = ps.executeQuery();
            int columns = rs.getMetaData().getColumnCount();
            ResultSetMetaData columnNames = rs.getMetaData();
            for (int x = 1; x <= columns; x++) {
                value.add(columnNames.getColumnName(x));
            }
            data.add(value);
            while (rs.next()) {
                value = new ArrayList<String>();
                for (int i = 1; i <= columns; i++) {
                    value.add(rs.getString(i));
                }
                data.add(value);
            }
            ;
            rs.close();
            ps.close();
            return SqlClient.convertToArray(data);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            if (this.connection != null) {
                try {
                    this.connection.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    @Override
    public String[][] selectMany(String sql, Object... conditions) {
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            List<ArrayList<String>> data = new ArrayList<>();
            ArrayList<String> value = new ArrayList<String>();
            int index = 1;
            if (conditions.length > 0 || conditions != null) {
                for (Object o : conditions) {
                    ps.setObject(index, o);
                    index++;
                }
            }
            ResultSet rs = ps.executeQuery();
            int columns = rs.getMetaData().getColumnCount();
            while (rs.next()) {
                value = new ArrayList<String>();
                for (int i = 1; i <= columns; i++) {
                    value.add(rs.getString(i));
                }
                data.add(value);
            }
            ;
            rs.close();
            ps.close();
            return SqlClient.convertToArray(data);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            if (this.connection != null) {
                try {
                    this.connection.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    @Override
    public String[][] selectOneWithColumnName(String sql, Object... conditions) {
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            List<ArrayList<String>> data = new ArrayList<ArrayList<String>>();
            ArrayList<String> value = new ArrayList<String>();
            int index = 1;
            if (conditions.length > 0 || conditions != null) {
                for (Object o : conditions) {
                    ps.setObject(index, o);
                    index++;
                }
            }
            ResultSet rs = ps.executeQuery();
            int columns = rs.getMetaData().getColumnCount();
            ResultSetMetaData columnNames = rs.getMetaData();
            for (int x = 1; x <= columns; x++) {
                value.add(columnNames.getColumnName(x));
            }
            data.add(value);
            if (rs.next()) {
                value = new ArrayList<String>();
                for (int i = 1; i <= columns; i++) {
                    value.add(rs.getString(i));
                }
                data.add(value);
            }
            ;
            rs.close();
            ps.close();
            return SqlClient.convertToArray(data);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            if (this.connection != null) {
                try {
                    this.connection.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    @Override
    public ResultSet single(String sql, Object... conditions) {
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            List<ArrayList<String>> data = new ArrayList<>();
            ArrayList<String> value = new ArrayList<String>();
            int index = 1;
            if (conditions.length > 0 || conditions != null) {
                for (Object o : conditions) {
                    ps.setObject(index, o);
                    index++;
                }
            }
            ResultSet rs = ps.executeQuery();
            ps.close();
            return rs;

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            if (this.connection != null) {
                try {
                    this.connection.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    @Override
    public ResultSet many(String sql, Object... conditions) {
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            List<ArrayList<String>> data = new ArrayList<>();
            ArrayList<String> value = new ArrayList<String>();
            int index = 1;
            if (conditions.length > 0 || conditions != null) {
                for (Object o : conditions) {
                    ps.setObject(index, o);
                    index++;
                }
            }
            ResultSet rs = ps.executeQuery();
            ps.close();
            return rs;

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            if (this.connection != null) {
                try {
                    this.connection.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    @Override
    public boolean isExist(String sql, Object... conditions) {
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            List<ArrayList<String>> data = new ArrayList<>();
            ArrayList<String> value = new ArrayList<String>();
            int index = 1;
            if (conditions.length > 0 || conditions != null) {
                for (Object o : conditions) {
                    ps.setObject(index, o);
                    index++;
                }
            }
            ResultSet rs = ps.executeQuery();
            return rs.next();

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            if (this.connection != null) {
                try {
                    this.connection.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        return false;
    }


    private static String[][] convertToArray(List<ArrayList<String>> data) {

        int rows = data.size();
        int cols = data.get(0).size();
        String[][] ss = new String[rows][cols];
        for (int x = 0; x < rows; x++) {
            for (int z = 0; z < cols; z++) {
                ss[x][z] = data.get(x).get(z);
            }
        }

        return ss;
    }

    private static String[] convertToArray(ArrayList<String> data) {

        int rows = data.size();
        String[] ss = new String[rows];
        for (int x = 0; x < rows; x++) {
            ss[x] = data.get(x);
        }

        return ss;
    }
}
