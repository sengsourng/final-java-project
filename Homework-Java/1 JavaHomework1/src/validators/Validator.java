package validators;

import java.util.regex.Pattern;

public class Validator {
    public static boolean isEmail(String email) {
        String regex = "^(.+)@(.+)$";
        Pattern pattern = Pattern.compile(regex);
        return pattern.matcher(email).find();
    }

    public static boolean isAlpha(String word) {
        Pattern pattern = Pattern.compile("^[a-zA-Z]*$");
        return pattern.matcher(word).find();
    }

    public static boolean isNumeric(String word) {
        String regex = "^[0-9]*$";
        Pattern pattern = Pattern.compile(regex);
        return pattern.matcher(word).find();

    }

    public static boolean isAlphaNumeric(String word) {
        String regex = "^[0-9a-zA-Z]*$";
        Pattern pattern = Pattern.compile(regex);
        return pattern.matcher(word).find();
    }

    public static boolean isDecimal(String input) {
        return Pattern.matches("^[-+]?\\d*[.]?\\d+|^[-+]?\\d+[.]?\\d*", input);
    }
}
