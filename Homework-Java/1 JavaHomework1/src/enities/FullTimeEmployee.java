package enities;

import constants.EmployeeConstant;
import services.DatabaseService;

import java.sql.SQLException;

public class FullTimeEmployee extends Employee {

    private double salary;

    public FullTimeEmployee() {

    }

    public FullTimeEmployee(String first_name, String last_name, String gender,
                            String email, int type, double salary, String address) {
        super(first_name, last_name, gender, email, type, address);
        this.salary = salary;

    }

    public FullTimeEmployee(int id, String first_name, String last_name, String gender, String email, int type, double salary, String address) {
        super(id, first_name, last_name, gender, email, type, address);
        this.salary = salary;
    }

    @Override
    public double salary() {
        return this.getSalary();
    }

    @Override
    public void createNewEmployee(Employee employee) throws SQLException {
        FullTimeEmployee emp = (FullTimeEmployee) employee;
        if (DatabaseService.connection().insert(EmployeeConstant.INSERT_EMPLOYEE, super.getFirst_name(), super.getLast_name(), super.getGender(), super.getEmail(), 1, emp.salary(), super.getAddress(), 0, 0)) {
            System.out.println("created");
        }
    }

    @Override
    public void updateEmployee(Employee employee, int id) throws SQLException {
        FullTimeEmployee emp = (FullTimeEmployee) employee;
        if (DatabaseService.connection().isExist(EmployeeConstant.SELECT_ONE_EMPLOYEE, id)) {
            if (DatabaseService.connection().update(EmployeeConstant.UPDATE_EMPLOYEE, super.getFirst_name(), super.getLast_name(),
                    super.getGender(), super.getEmail(), 2, emp.salary(), super.getAddress(), 0, 0, id)) {
                System.out.println("updated.");
            }
        } else {
            System.out.println("There is no any record to update.");
        }
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

}
