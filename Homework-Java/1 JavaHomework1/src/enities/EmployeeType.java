package enities;

public enum EmployeeType {
    FullTime,
    PartTime,
    Hourly
}
