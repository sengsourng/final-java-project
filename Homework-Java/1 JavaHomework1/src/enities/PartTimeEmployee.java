package enities;

import constants.EmployeeConstant;
import services.DatabaseService;
import validators.Validator;

import java.sql.SQLException;

public class PartTimeEmployee extends Employee {

    private double rate;
    private double hours;

    public PartTimeEmployee() {
    }

    public PartTimeEmployee(String first_name, String last_name, String gender,
                            String email, int type, double rate, double hours, String address) {
        super(first_name, last_name, gender, email, type, address);
        this.rate = rate;
        this.hours = hours;
    }

    public PartTimeEmployee(int id, String first_name, String last_name, String gender, String email, int type,
                            double rate, double hours, String address) {
        super(id, first_name, last_name, gender, email, type, address);
        this.rate = rate;
        this.hours = hours;
    }

    @Override
    public double salary() {
        return getRate() * getHours();
    }

    @Override
    public void createNewEmployee(Employee employee) throws SQLException {
        PartTimeEmployee emp = (PartTimeEmployee) employee;
        if (DatabaseService.mysqlInstance().insert(EmployeeConstant.INSERT_EMPLOYEE, emp.getFirst_name(), emp.getLast_name(),
                emp.getGender(), emp.getEmail(), 2, emp.salary(), emp.getAddress(), emp.rate, emp.hours)) {
            System.out.println("created.");
        }
    }

    @Override
    public void updateEmployee(Employee employee, int id) throws SQLException {
        PartTimeEmployee emp = (PartTimeEmployee) employee;
        if (DatabaseService.connection().isExist(EmployeeConstant.SELECT_ONE_EMPLOYEE, id)) {
            if (DatabaseService.connection().update(EmployeeConstant.UPDATE_EMPLOYEE, emp.getFirst_name(), emp.getLast_name(),
                    emp.getGender(), emp.getEmail(), 2, emp.salary(), emp.getAddress(), emp.rate, emp.hours, id)) {
                System.out.println("updated.");
            }
        } else {
            System.out.println("There is no any record to update.");
        }

    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public double getHours() {
        return hours;
    }

    public void setHours(double hours) {
        this.hours = hours;
    }

}
