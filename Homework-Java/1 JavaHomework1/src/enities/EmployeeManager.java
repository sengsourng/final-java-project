package enities;

import constants.EmployeeConstant;
import services.DatabaseService;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

public class EmployeeManager {
    private ArrayList<Employee> employees = null;
    private int type;

    private Employee employee = null;
    private PartTimeEmployee partTimeEmployee = null;
    private FullTimeEmployee fullTimeEmployee = null;

    public void addEmployee() {
        try {
//            Scanner scanner=new Scanner(System.in);
//            System.out.print("Employee Type \n1:Full,\n2:for Part Time:");
//            this.type=scanner.nextInt();
//            this.input(this.type);
            setEmployee();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void removeEmployee() {
        Scanner scanner = new Scanner(System.in);
        try {
            System.out.print("choose employee id to delete:");
            int id = scanner.nextInt();
            new FullTimeEmployee().removeEmployee(id);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void updateEmployee() {
        try {
            Scanner scanner = new Scanner(System.in);
            System.out.println("choose employee for update by id:");
            int _id = scanner.nextInt();
            if (DatabaseService.connection().isExist(EmployeeConstant.SELECT_ONE_EMPLOYEE, _id)) {
                var type = DatabaseService.connection().find("SELECT type FROM employees WHERE emp_id=?", _id);
                this.inputUpdate(Integer.parseInt(type[0]), _id);
            }
            System.out.println("There is no record for update.");
            return;

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void viewAllEmployee() {

        Employee emp = new PartTimeEmployee();
        emp.viewAllEmployees();
    }

    public void viewOneEmployee() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Employee Id:");
        int id = scanner.nextInt();
        new FullTimeEmployee().viewSingleEmployee(id);

    }

    private static EmployeeManager employeeManager;

    public static EmployeeManager getInstance() {
        if (employeeManager == null) {
            employeeManager = new EmployeeManager();
        }
        return employeeManager;
    }

    private void input(int type) throws IOException, SQLException {
//        if(type==1){
//            this.getFullEmployee();
//            new FullTimeEmployee().createNewEmployee(employee);
//        }
//        if(type==2){
//            this.getPartTimeEmployee();
//            new PartTimeEmployee().createNewEmployee(employee);
//        }

    }

    private void inputUpdate(int type, int id) throws IOException, SQLException {
        if (type == 1) {
            this.getFullEmployee();
            new FullTimeEmployee().updateEmployee(employee, id);
        }
        if (type == 2) {
            this.getPartTimeEmployee();
            new PartTimeEmployee().updateEmployee(employee, id);
        }

    }

    private void getPartTimeEmployee() throws IOException {
        PartTimeEmployee emp = new PartTimeEmployee();
        Scanner scanner = new Scanner(System.in);
        System.out.print("First Name:");
        emp.setFirst_name(scanner.next());
        System.out.print("Last Name:");
        emp.setLast_name(scanner.next());
        System.out.print("Gender:");
        emp.setGender(scanner.next());
        System.out.print("Email:");
        String email = scanner.next();
        System.out.print("Rate:");
        emp.setRate(scanner.nextDouble());
        System.out.print("Hour:");
        emp.setHours(scanner.nextDouble());
        System.out.print("Address:");
        scanner.nextLine();
        emp.setAddress(scanner.nextLine());
        employee = emp;
    }

    private void getFullEmployee() throws IOException {
        FullTimeEmployee emp = new FullTimeEmployee();
        Scanner scanner = new Scanner(System.in);
        System.out.print("First Name:");
        emp.setFirst_name(scanner.next());
        System.out.print("Last Name:");
        emp.setLast_name(scanner.next());
        System.out.print("Gender:");
        emp.setGender(scanner.next());
        System.out.print("Email:");
        String email = scanner.next();
        System.out.print("Salary:");
        emp.setSalary(scanner.nextDouble());
        System.out.print("Address:");
        scanner.nextLine();
        emp.setAddress(scanner.nextLine());
        employee = emp;
    }

    private void setEmployee() throws IOException, SQLException {
        Scanner scanner = new Scanner(System.in);
        System.out.print("First Name:");
        String fname = scanner.next();
        System.out.print("Last Name:");
        String lname = scanner.next();
        System.out.print("Gender:");
        String gender = scanner.next();
        System.out.print("Email:");
        String email = scanner.next();
        System.out.print("Employee Type:");
        this.type = scanner.nextInt();
        double rate = 0;
        double hours = 0;
        double salary = 0;
        if (this.type == 2) {
            System.out.print("Rate:");
            rate = scanner.nextDouble();
            System.out.print("Hour:");
            hours = scanner.nextDouble();
        } else {
            System.out.print("Salary:");
            salary = scanner.nextDouble();
        }
        System.out.print("Address:");
        scanner.nextLine();
        String address = scanner.nextLine();
        if (this.type == 2) {
            partTimeEmployee = new PartTimeEmployee(fname, lname, gender, email, this.type, rate, hours, address);
            partTimeEmployee.createNewEmployee(partTimeEmployee);
        } else {
            fullTimeEmployee = new FullTimeEmployee(fname, lname, gender, email, this.type, salary, address);
            fullTimeEmployee.createNewEmployee(fullTimeEmployee);
        }
    }

}
