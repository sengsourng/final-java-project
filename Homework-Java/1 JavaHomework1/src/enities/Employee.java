package enities;

import constants.EmployeeConstant;
import services.DatabaseService;
import validators.Validator;
import views.Table;

import java.sql.SQLException;

public abstract class Employee {
    private int emp_id;
    private String first_name;
    private String last_name;
    private String gender;
    private String email;
    private int type;
    private String address;

    public Employee() {
    }

    ;

    public Employee(String firstName, String lastName, String gender, String email, int type, String address) {
        this.first_name = firstName;
        this.last_name = lastName;
        this.gender = gender;
        this.email = email;
        this.type = type;
        this.address = address;
    }

    public Employee(int id, String firstName, String lastName, String gender, String email, int type, String address) {
        this(firstName, lastName, gender, email, type, address);
        this.emp_id = id;
    }

    public void viewAllEmployees() {
        try {
            var data = DatabaseService.connection().selectManyWithColumnName(EmployeeConstant.SELECT_ALL_EMPLOYEE);
            Table.printTable(data);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void viewSingleEmployee(int id) {
        try {
            if (DatabaseService.connection().isExist(EmployeeConstant.SELECT_ONE_EMPLOYEE, id)) {
                Table.printTable(DatabaseService.connection().selectOneWithColumnName(EmployeeConstant.SELECT_ONE_EMPLOYEE, id));
            }
            System.out.println("There is no record to view.");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void removeEmployee(int id) throws SQLException {
        if (DatabaseService.connection().isExist(EmployeeConstant.SELECT_ONE_EMPLOYEE, id)) {
            if (DatabaseService.connection().delete(EmployeeConstant.REMOVE_EMPLOYEE, id)) {
                System.out.println("one row was deleted.");
            }
        } else {
            System.out.println("There is no any record to delete.");
        }
    }

    public int getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(int emp_id) {
        this.emp_id = emp_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        if (Validator.isAlpha(first_name)) {
            this.first_name = first_name;
        } else {
            System.out.println("name accept only alphabet.");
        }

    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        if (Validator.isAlpha(last_name)) {
            this.last_name = last_name;
        } else {
            System.out.println("name accept only alphabet.");
        }

    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        if (Validator.isEmail(email)) {
            this.email = email;
        } else {
            System.out.println("Email is not valid.");
        }
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    //abtract methods
    public abstract double salary();

    public abstract void createNewEmployee(Employee employee) throws SQLException;

    public abstract void updateEmployee(Employee employee, int id) throws SQLException;
}
