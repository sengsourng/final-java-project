package views;

import java.sql.SQLException;
import java.util.*;
import java.util.stream.Stream;

public class Table {
    //    private static List<ArrayList<String>> dataDisplay=new ArrayList<ArrayList<String>>();
//    public  static  void printTable(ArrayList<String> columnNames,List<ArrayList<String>> data) throws Exception {
//        if(columnNames.size()==0 && data.size()==0){
//            return;
//        }else{
//            if(columnNames.size() !=data.size()){
//                throw  new Exception("column name and data not in smae size.");
//            }
//        }
//        Table.dataDisplay.add(columnNames);
//        for(var d:data){
//            dataDisplay.add(d);
//        }
//        var table= Table.convertToArray(Table.dataDisplay);
//        boolean leftJustifiedRows = false;
//        Map<Integer, Integer> columnLengths = new HashMap<>();
//        Arrays.stream(table).forEach(a -> Stream.iterate(0, (i -> i < a.length), (i -> ++i)).forEach(i -> {
//            if (columnLengths.get(i) == null) {
//                columnLengths.put(i, 0);
//            }
//            if (columnLengths.get(i)< a[i].length()) {
//                columnLengths.put(i, a[i].length());
//            }
//        }));
//        /*
//         * Prepare format String
//         */
//        final StringBuilder formatString = new StringBuilder("");
//        String flag = leftJustifiedRows ? "-" : "";
//        columnLengths.entrySet().stream().forEach(e -> formatString.append("| %" + flag + e.getValue() + "s "));
//        formatString.append("|\n");
//        /*
//         * Prepare line for top, bottom & below header row.
//         */
//        String line = columnLengths.entrySet().stream().reduce("", (ln, b) -> {
//            String templn = "+-";
//            templn = templn + Stream.iterate(0, (i -> i < b.getValue()), (i -> ++i)).reduce("", (ln1, b1) -> ln1 + "-",
//                    (a1, b1) -> a1 + b1);
//            templn = templn + "-";
//            return ln + templn;
//        }, (a, b) -> a + b);
//        line = line + "+\n";
//
//        /*
//         * Print table
//         */
//        System.out.print(line);
//        Arrays.stream(table).limit(1).forEach(a -> System.out.printf(formatString.toString(), a));
//        System.out.print(line);
//
//        Stream.iterate(1, (i -> i < table.length), (i -> ++i))
//                .forEach(a -> System.out.printf(formatString.toString(), table[a]));
//        System.out.print(line);
//    }
////    public  static  void printTable(List<ArrayList<String>> data){
////        var table= Table.convertToArray(data);
////        boolean leftJustifiedRows = false;
////        Map<Integer, Integer> columnLengths = new HashMap<>();
////        Arrays.stream(table).forEach(a -> Stream.iterate(0, (i -> i < a.length), (i -> ++i)).forEach(i -> {
////            if (columnLengths.get(i) == null) {
////                columnLengths.put(i, 0);
////            }
////            if (columnLengths.get(i)< a[i].length()) {
////                columnLengths.put(i, a[i].length());
////            }
////        }));
////        /*
////         * Prepare format String
////         */
////        final StringBuilder formatString = new StringBuilder("");
////        String flag = leftJustifiedRows ? "-" : "";
////        columnLengths.entrySet().stream().forEach(e -> formatString.append("| %" + flag + e.getValue() + "s "));
////        formatString.append("|\n");
////        /*
////         * Prepare line for top, bottom & below header row.
////         */
////        String line = columnLengths.entrySet().stream().reduce("", (ln, b) -> {
////            String templn = "+-";
////            templn = templn + Stream.iterate(0, (i -> i < b.getValue()), (i -> ++i)).reduce("", (ln1, b1) -> ln1 + "-",
////                    (a1, b1) -> a1 + b1);
////            templn = templn + "-";
////            return ln + templn;
////        }, (a, b) -> a + b);
////        line = line + "+\n";
////
////        /*
////         * Print table
////         */
////        System.out.print(line);
////        Arrays.stream(table).limit(1).forEach(a -> System.out.printf(formatString.toString(), a));
////        System.out.print(line);
////
////        Stream.iterate(1, (i -> i < table.length), (i -> ++i))
////                .forEach(a -> System.out.printf(formatString.toString(), table[a]));
////        System.out.print(line);
////    }
    public static void printTable(String[][] table) throws SQLException {
        boolean leftJustifiedRows = false;
        System.out.println(table.length);
        Map<Integer, Integer> columnLengths = new HashMap<>();
        for (int x = 0; x < table.length; x++) {
            String[] temp = table[x];
            for (int y = 0; y < temp.length; y++) {
                if (columnLengths.get(y) == null) {
                    columnLengths.put(y, 0);
                }
                if (columnLengths.get(y) < (temp[y] == null ? 0 : temp[y].length())) {
                    columnLengths.put(y, (temp[y] == null ? 0 : temp[y].length()));
                }
            }
        }
        final StringBuilder formatString = new StringBuilder("");
        String flag = leftJustifiedRows ? "-" : "";
        columnLengths.entrySet().stream().forEach(e -> formatString.append("| %" + flag + e.getValue() + "s "));
        formatString.append("|\n");

        String line = columnLengths.entrySet().stream().reduce("", (ln, b) -> {
            String templn = "+-";
            templn = templn + Stream.iterate(0, (i -> i < b.getValue()), (i -> ++i)).reduce("", (ln1, b1) -> ln1 + "-",
                    (a1, b1) -> a1 + b1);
            templn = templn + "-";
            return ln + templn;
        }, (a, b) -> a + b);
        line = line + "+\n";
        System.out.print(line);
        Arrays.stream(table).limit(1).forEach(a -> System.out.printf(formatString.toString(), a));
        System.out.print(line);

        Stream.iterate(1, (i -> i < table.length), (i -> ++i))
                .forEach(a -> System.out.printf(formatString.toString(), table[a]));
        System.out.print(line);
    }


    private static String[][] convertToArray(List<ArrayList<String>> data) {

        int rows = data.size();
        int cols = data.get(0).size();
        String[][] ss = new String[rows][cols];
        for (int x = 0; x < rows; x++) {
            for (int z = 0; z < cols; z++) {
                ss[x][z] = data.get(x).get(z);
            }
        }

        return ss;
    }

}
