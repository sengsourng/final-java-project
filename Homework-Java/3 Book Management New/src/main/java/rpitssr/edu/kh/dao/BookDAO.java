package rpitssr.edu.kh.dao;

import java.util.List;

import rpitssr.edu.kh.modal.Book;

public interface BookDAO {
	
	List<Book> get();
	
	Book get(int id);
	
	boolean save(Book book);
	
	boolean delete(int id);
	
	boolean update(Book book);
	
}
