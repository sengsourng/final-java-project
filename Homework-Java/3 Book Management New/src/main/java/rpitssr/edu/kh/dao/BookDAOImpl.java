package rpitssr.edu.kh.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import rpitssr.edu.kh.modal.Book;
import rpitssr.edu.kh.util.DBConnectionUtil;

public class BookDAOImpl implements BookDAO{

	Connection connection = null;
	ResultSet resultSet = null;
	Statement statement = null;
	PreparedStatement preparedStatement = null;
	
	@Override
	public List<Book> get() {
		List<Book> list = null;
		Book book = null;
		
		try {
			list = new ArrayList<Book>();
			String sql = "SELECT * FROM tblbook";
			connection = DBConnectionUtil.openConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sql);
			while(resultSet.next()) {
				book = new Book();
				book.setId(resultSet.getInt("id"));
				book.setTitle(resultSet.getString("title"));
				book.setAuthor(resultSet.getString("author"));
				book.setPrice(Double.parseDouble(resultSet.getString("price")));
				list.add(book);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public Book get(int id) {
		Book book = null;
		try {
			book = new Book();
			String sql = "SELECT * FROM tblbook where id="+id;
			connection = DBConnectionUtil.openConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sql);
			if(resultSet.next()) {
				book.setId(resultSet.getInt("id"));
				book.setTitle(resultSet.getString("title"));
				book.setAuthor(resultSet.getString("author"));
				book.setPrice(Double.parseDouble(resultSet.getString("price")));
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return book;
	}

	@Override
	public boolean save(Book book) {
		boolean flag = false;
		try {
			String sql = "INSERT INTO tblbook(title, author, price)VALUES"
					+ "('"+book.getTitle()+"', '"+book.getAuthor()+"', '"+book.getPrice()+"')";
			connection = DBConnectionUtil.openConnection();
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.executeUpdate();
			flag = true;
		}catch(SQLException ex) {
			ex.printStackTrace();
		}
		return flag;
	}

	@Override
	public boolean delete(int id) {
		boolean flag = false;
		try {
			String sql = "DELETE FROM tblbook where id="+id;
			connection = DBConnectionUtil.openConnection();
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.executeUpdate();
			flag = true;
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return flag;
	}

	@Override
	public boolean update(Book book) {
		boolean flag = false;
		try {
			String sql = "UPDATE tblbook SET title = '"+book.getTitle()+"', "
					+ "author = '"+book.getAuthor()+"', price = '"+book.getPrice()+"' where id="+book.getId();
			connection = DBConnectionUtil.openConnection();
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.executeUpdate();
			flag = true;
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return flag;
	}
}

