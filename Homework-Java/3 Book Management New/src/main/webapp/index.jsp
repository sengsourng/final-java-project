<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Home</title>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

</head>
<body>
<jsp:include page="common/header.jsp"></jsp:include>

	<div class = "container">
		
		<h1 align = center>Books Management</h1>
		<hr/>
		<div>
		  <p>${NOTIFICATION}</p>
		</div>
		<p>
			<button class = "btn btn-primary" onclick="window.location.href = '${pageContext.request.contextPath}/BookController?action=LIST'">View All Book</button>
			
		</p>
	
		
		
	</div>

	
	
<jsp:include page="common/footer.jsp"></jsp:include>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>
</html>