<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>

	<%
	String Email = request.getParameter("email");
	String Password = request.getParameter("password");
	String Birthday = request.getParameter("birthday");
	String NewStudent = request.getParameter("newstudent");
	String College = request.getParameter("college");
	String Major = request.getParameter("major");
	String FavoriteColor = request.getParameter("favoritecolor");
	String TermAndConditions = request.getParameter("termandcondtion");
	%>
	
<html>
	
<head>
	<style>
		#idh1{text-align: center;}
		#idh1{color: red;}
		
		body{background-color: <%= FavoriteColor %>}	
		th{text-align: left;}
		.th1{text-align: center;}
		
	</style>
<meta charset="ISO-8859-1">
<title>My JSP</title>
</head>
<!-- Comment code -->
<body>
		  
	<!--<h1 id="idh1">Submit Data</h1>-->
		
	<table border="0">
		<thead>
		    <tr>
			    <th colspan='2'><h3 style='text-align:center'>Submitted data</h3></th>
			</tr>
		 
		    <tr>
			    <th class="th1">Field:</th>
			    <th class="th1">Value:</th>
		    </tr>
		</thead>
	
		<tbody>
		    <tr>
			      <th>Email</th>
			      <td><%= Email  %></td>
		    </tr>
		    
		    <tr>
			      <th>Password</th>
			      <td><%= Password  %></td>
		    </tr>
		    
		    <tr>
			      <th>Birthday</th>
			      <td><%= Birthday  %></td>
		    </tr>
		    
		    <tr>
			      <th>New student</th>
			      <td><%= NewStudent %></td>
		    </tr>
		    
		    <tr>
			      <th>College</th>
			      <td><%= College  %></td>
		    </tr>
		    
		    <tr>
			      <th>Major</th>
			      <td><%= Major  %></td>
			</tr>
		    
		    <tr>
			      <th>Favorite color</th>
			      <td><%= FavoriteColor  %></td>
		    </tr>
		    
		    <tr>
			      <th>Term and conditions:</th>
			      <td><%= TermAndConditions   %></td>
		    </tr>
	  	</tbody>
	</table>
		
</body>
</html>