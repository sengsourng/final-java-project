package com.sourngtech.springboothomework6;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springboothomework6Application {

	public static void main(String[] args) {
		SpringApplication.run(Springboothomework6Application.class, args);
	}

}
