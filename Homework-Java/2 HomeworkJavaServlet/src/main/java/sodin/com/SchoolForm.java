package sodin.com;


import java.io.IOException;
import java.io.PrintWriter;

import org.apache.tomcat.websocket.BackgroundProcess;

import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet ("/SchoolName")
public class SchoolForm extends HttpServlet{
	public void doPost(HttpServletRequest request, HttpServletResponse respone) throws IOException{
				
		PrintWriter out=respone.getWriter();
		
		out.println("<!DOCTYPE html>");
		
		String Email=request.getParameter("email");
		String Password=request.getParameter("pwd");
		String birthday=request.getParameter("birthday");
		String newstudent=request.getParameter("NewStudent");
		String College=request.getParameter("college");
		String Major=request.getParameter("major");
		String FavoriteColor=request.getParameter("color");
		String TermAndCondition=request.getParameter("term");
		
	out.println("<html>");
	out.println("<head>");
	out.println("<title>Submit Form</title>");
//Background Color		
		out.println("<style>");
			out.println(".bg{background-color:" + FavoriteColor + ";}");
//			out.println(".hi{text-align:right;}");
		out.println("</style>");

	out.println("</head>");
		
	out.println("<body class='bg'>");
		 			
//			out.println("<h1><strong>Submit Data</strong></h1>");
		
		out.println("<table border= 1px>");
			
			out.println("<tr>");
				out.println("<th colspan ='2'><h3 style='text-align:center'>Submitted data</h3></th>");
			out.println("</tr>");
		
			out.println("<tr>");
				out.println("<th>Field</th>");
				out.println("<th>Value</th>");
			out.println("</tr>");
			
			out.println("<tr>");
				out.println("<td>Email:</td>");
				out.println("<td>" + Email + "</td>");
			out.println("</tr>");
			
			out.println("<tr>");
				out.println("<td>Password:</td>");
				out.println("<td>" + Password+ "</td>");
			out.println("</tr>");
			
			out.println("<tr>");
				out.println("<td>Birthday:</td>");
				out.println("<td>" + birthday + "</td>");
			out.println("</tr>");
			
			out.println("<tr>");
				out.println("<td> New Student: </td>");
				out.println("<td>" + newstudent + "</td>");
			out.println("</tr>");
			
			out.println("<tr>");
				out.println("<td> College </td>");
				out.println("<td>" + College + "</td>");		
			out.println("</tr>");
			
			out.println("<tr>");
				out.println("<td>Major:</td>");
				out.println("<td>" + Major + "</td>");
			out.println("</tr>");
			
			out.println("<tr>");
				out.println("<td>Favorite Color:</td>");
				out.println("<td>" + FavoriteColor + "</td>");
			out.println("</tr>");
			
			out.println("<tr>");
				out.println("<td>Term&Conditions:</td>");
				out.println("<td>" + TermAndCondition + "</td>");
			out.println("</tr>");
		
		out.println("</table>");
		
				
//		out.println("Email:"+ Email + "<br/>");
//		out.println("Password:" + Password + "<br/>");
		
		
		out.println("</body>");
	out.println("</html>");
		
//		out.println("Email"+ Email);
//		out.println("Password"+ Password);
		
		
	}
}
