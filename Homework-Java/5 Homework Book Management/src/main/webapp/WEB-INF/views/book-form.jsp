<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
 pageEncoding="ISO-8859-1"%>
 
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add New Book</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<style>
footer {
    position: fixed;
    height: 50px;
    bottom: 0;
    width: 100%;
}
</style>

</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary shadow p-3 mb-5 bg-body">
  <a class="navbar-brand" href="#">Homework5 Book Management</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
     <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="list">Home<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="create">Add New Book</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="list">List Books</a>
      </li>
      
    </ul>
    
    
    
  </div>
</nav>
 <div class="container">
  <div class="col-md-offset-5 col-md-12">
  
   <div class="card">
    <div class="card-heading">
    	<h3 class="card-title text-center pt-3">Add New Book</h3>
    </div>
    <div class="card-body">
	     <form:form action="saveBook" cssClass="form-horizontal"
	      method="post" modelAttribute="book">
	      <!-- need to associate this data with customer id -->
	      <form:hidden path="id" />
	      <div class="form-group">
	       <label for="title" class="col-md-3 control-label">
	        Title</label>
	       <div class="col-md-12">
	        <form:input path="title" cssClass="form-control" placeholder="Enter Book Title" />
	       </div>
	      </div>
	      <div class="form-group">
	       <label for="author" class="col-md-3 control-label">
	        Author</label>
	       <div class="col-md-12">
	        <form:input path="author" cssClass="form-control" placeholder="Enter Book Author" />
	       </div>
	      </div>
	
	      <div class="form-group">
	       <label for="price" class="col-md-3 control-label">Price</label>
	       <div class="col-md-12">
	        <form:input path="price" cssClass="form-control" placeholder="Enter Book Price(ex. 3.5)" />
	       </div>
	      </div>
	
	      <div class="form-group">
	       <!-- Button -->
	       <div class="col-md-12">
	        <form:button Class="btn btn-primary float-right">Save Book</form:button>
	       </div>
	      </div>
	
	     </form:form>
    </div>
   </div>
  </div>
 </div>
 
  <footer class="footer navbar-fixed-bottom bg-primary text-white pt-3">
    <div class="text-center">Designed By : SENG Sourng | MIT @ BBU-SR | Site: www.sourngtech.com</div>
</footer>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>
</html>