<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
 
  <%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>List of Books</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

	<style>
		footer {
		    position: fixed;
		    height: 50px;
		    bottom: 0;
		    width: 100%;
		}
	</style>

</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary shadow p-3 mb-5 bg-body">
  <a class="navbar-brand" href="#">Homework5 Book Management</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="${pageContext.request.contextPath}/list">Home<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="${pageContext.request.contextPath}/create">Add New Book</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="${pageContext.request.contextPath}/list">List Books</a>
      </li>
      
    </ul>
    
    
    
  </div>
</nav>
 <div class="container">
  <div class="col-md-offset-5 col-md-12">
     <div class="panel panel-info">
    <div class="panel-heading  mt-4">
     <h3 class="pt-3">List All Books  <input type="button" value="Add Book"
    onclick="window.location.href='create'; return false;"
    class="btn btn-success float-right" /></h3>
    
    </div>
    <div class="panel-body">
    
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th>ID</th>
					<th>Title</th>
					<th>Author</th>
					<th>Price</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${listBook}" var="book">
						 <!-- construct an "update" link with id -->
					       <c:url var="updateLink" value="/updateForm">
					        <c:param name="BookId" value="${book.id}" />
					       </c:url>
					
					       <!-- construct an "delete" link with  id -->
					       <c:url var="deleteLink" value="/delete">
					        <c:param name="BookId" value="${book.id}" />
					       </c:url>
					<tr>
						<td><c:out value="${book.id}" /></td>
						<td><c:out value="${book.title}" /></td>
						<td><c:out value="${book.author}" /></td>
						<td><c:out value="${book.price}" /></td>
						<td> <a class="btn btn-success btn-sm" href="${updateLink}">Update</a>  <a class="btn btn-danger btn-sm" href="${deleteLink}"
         				onclick="if (!(confirm('Are you sure you want to delete this book?'))) return false">Delete</a>
       				 </td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
</div>
   </div>
  </div>

 </div>
 
  <footer class="footer navbar-fixed-bottom bg-primary text-white pt-3">
    <div class="text-center">Designed By : SENG Sourng | MIT @ BBU-SR | Site: www.sourngtech.com</div>
</footer>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>
</html>