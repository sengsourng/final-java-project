<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
 

  
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>List of Books</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<style>
footer {
    position: fixed;
    height: 50px;
    bottom: 0;
    width: 100%;
}
</style>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary shadow p-3 mb-5 bg-body">
  <a class="navbar-brand" href="#">Homework5 Book Management</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="list">Home<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="create">Add New Book</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="list">List Books</a>
      </li>
      
    </ul>
    
    
    
  </div>
</nav>
 <div class="container">
  <div class="col-md-offset-1 col-md-10">
	<h2 class="text-center">Welcome to Book Store System</h2>
   <hr />

   
   <div class="panel panel-info">
    <div class="panel-heading">     
    </div>
    <div class="panel-body">
    	
    	<h3>Student Name : SENG Sourng</h3>
    	<h3>Subject Name : Advance Java Programming</h3>
		
	</div>
   </div>
  </div>
 </div>
 
 <footer class="footer navbar-fixed-bottom bg-primary text-white pt-3">
    <div class="text-center">Designed By : SENG Sourng | MIT @ BBU-SR | Site: www.sourngtech.com</div>
</footer>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>
</html>