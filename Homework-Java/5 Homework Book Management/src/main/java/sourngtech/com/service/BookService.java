package sourngtech.com.service;

import java.util.List;

import sourngtech.com.model.Book;

public interface BookService {
	List<Book> getAll();
	Book get(int id);
	void add(Book book);
	void delete(int id);
	void update(int id, Book book);
}
