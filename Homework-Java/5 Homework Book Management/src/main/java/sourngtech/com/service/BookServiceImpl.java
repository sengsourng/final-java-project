package sourngtech.com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sourngtech.com.model.Book;
import sourngtech.com.respository.BookDaoImpl;

@Service
public class BookServiceImpl implements BookService {
	@Autowired
	private BookDaoImpl bookDaoImpl;
	public List<Book> getAll() {
		return bookDaoImpl.getAllBook();
	}

	public Book get(int id) {
		return bookDaoImpl.getBookById(id);
	}

	public void add(Book book) {
		bookDaoImpl.addBook(book);
	}

	public void delete(int id) {
		bookDaoImpl.deleteBook(id);
	}

	public void update(int id, Book book) {
		bookDaoImpl.updateBook(id, book);
	}

}
