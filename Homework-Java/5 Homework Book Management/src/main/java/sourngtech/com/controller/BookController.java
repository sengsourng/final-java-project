package sourngtech.com.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.servlet.ModelAndView;

import sourngtech.com.model.Book;
import sourngtech.com.service.BookServiceImpl;

@Controller
@RequestMapping("/")
public class BookController {

	@Autowired
	private BookServiceImpl bookService;
	
	@GetMapping("/")
	
	public ModelAndView index(ModelAndView model) {
		List<Book> books=bookService.getAll();
		model.addObject("bookList", books);
		model.setViewName("index");
		return model;
		
	}
	
	@GetMapping("/list")  
	public ModelAndView listBook(ModelAndView model) {
		List<Book> books=bookService.getAll();
		model.addObject("listBook", books);
		model.setViewName("book-list");
		return model;
		
	}
    
	@GetMapping("/create")
    public String showFormForAdd(Model theModel) {
        Book theBook = new Book();
        theModel.addAttribute("book", theBook);
        return "book-form";
    }

    @PostMapping("/saveBook")
    public String saveBook(@ModelAttribute("book") Book theBook) {
    	System.out.println(theBook.getId());
    	
    	if(theBook.getId() == 0) {
    		bookService.add(theBook);
    	}else {
    		bookService.update(theBook.getId(), theBook);
    	} 
    	
        return "redirect:/list";
    }

    @GetMapping("/updateForm")
    public String showFormForUpdate(@RequestParam("BookId") int theId,
        Model theModel) {
        Book theBook = bookService.get(theId);
        theModel.addAttribute("book", theBook);
        return "book-form";
    }

    @GetMapping("/delete")
    public String deleteBook(@RequestParam("BookId") int theId) {
    	bookService.delete(theId);
        return "redirect:/list";
    }
	
}
