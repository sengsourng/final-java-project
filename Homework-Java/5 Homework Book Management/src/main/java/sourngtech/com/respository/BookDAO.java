package sourngtech.com.respository;

import java.util.List;

import sourngtech.com.model.Book;

public interface BookDAO {
	List<Book> getAllBook();
	Book getBookById(int id);
	boolean addBook(Book book);
	boolean updateBook(int id,Book book);
	boolean deleteBook(int id);
}
