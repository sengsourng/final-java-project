package sourngtech.com.respository;

import java.util.List;
import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import sourngtech.com.BookMapper;
import sourngtech.com.model.Book;

@Repository
public class BookDaoImpl implements BookDAO {
	private JdbcTemplate jdbcTemplate;
	
	private final String SQL_FIND_BOOK = "SELECT * FROM tbl_book where id=?";
	private final String SQL_DELETE_BOOK = "DELETE FROM tbl_book where id=?";
	private final String SQL_UPDATE_BOOK = "UPDATE tbl_book SET title=?, author=?, price=? where id=?";
	private final String SQL_GET_ALL_BOOK = "SELECT * FROM tbl_book";
	private final String SQL_INSERT_BOOK = "INSERT INTO tbl_book(title,author,price) VALUES(?,?,?)";
	
	public BookDaoImpl(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}

	public List<Book> getAllBook() {
		return jdbcTemplate.query(SQL_GET_ALL_BOOK, new BookMapper());
		/*List<Book> list = jdbcTemplate.query("SELECT * FROM tbl_book", new RowMapper<Book>() {

			public Book mapRow(ResultSet rs, int rowNum) throws SQLException {
				Book book=new Book();
				book.setId(rs.getInt("id"));
				book.setTitle(rs.getString("title"));
				book.setAuthor(rs.getString("author"));
				book.setPrice(rs.getDouble("price"));
				return book;
				
			}
			
		});
		return list;
		*/
	}

	public Book getBookById(int id) {
		return jdbcTemplate.queryForObject(SQL_FIND_BOOK, new Object[] {id}, new BookMapper() );
	}

	public boolean addBook(Book book) {
		return jdbcTemplate.update(SQL_INSERT_BOOK,book.getTitle(),book.getAuthor(),book.getPrice())>0;
	}

	public boolean updateBook(int id, Book book) {
		return jdbcTemplate.update(SQL_UPDATE_BOOK,book.getTitle(),book.getAuthor(),book.getPrice(),book.getId())>0;
	}

	public boolean deleteBook(int id) {
		return jdbcTemplate.update(SQL_DELETE_BOOK,id)>0;
	}

}
