package sourngtech.com.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import sourngtech.com.respository.BookDAO;
import sourngtech.com.respository.BookDaoImpl;
import sourngtech.com.service.BookService;
import sourngtech.com.service.BookServiceImpl;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"sourngtech"
		+ ""
		+ ""
		+ ""
		+ ""
		+ ".com"})
@PropertySource("classpath:database.properties")
public class WebMVCConfig {
	@Autowired
    private Environment environment;
	
     
	
	@Bean
	public InternalResourceViewResolver viewResource() {
		InternalResourceViewResolver vr = new InternalResourceViewResolver();
		vr.setPrefix("WEB-INF/views/");
		vr.setSuffix(".jsp");
		return vr;
	}

    @Bean
    public DriverManagerDataSource getDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(environment.getRequiredProperty("driver"));
        dataSource.setUrl(environment.getRequiredProperty("url"));
        dataSource.setUsername(environment.getRequiredProperty("dbuser"));
        dataSource.setPassword(environment.getRequiredProperty("dbpassword"));
        return dataSource;
    }

	@Bean
	public BookDAO getBookDao() {
		return new BookDaoImpl(getDataSource());
	}
	
	@Bean
	public BookService getBookService() {
		return new BookServiceImpl();
	}
}
