<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add New Book</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>

<body>

<%
	if(session.getAttribute("username")==null){
		response.sendRedirect("../Login/login.jsp");
	}
%>

<jsp:include page="../common/header.jsp"></jsp:include>

	<div class="container">
	
		<h1>Add New Book </h1>
	
		<div class = "row">
			<div class = "col-md-4">
				<form action = "${pageContext.request.contextPath}/BookController" method="POST">
					<div class = "form-group">
						<input type="text" class="form-control" name="title" placeholder="Enter title" value="${book.title}"/>
					</div>
				
					<div class = "form-group">
						<input type="text" class="form-control" name="author" placeholder="Enter author name" value="${book.author}"/>
					</div>
				
					<div class="form-group">
					  	<input type="number" class="form-control" name="price" placeholder="Enter price" value="${book.price}"/>
					</div>
				
					<input type = "hidden" name = "id" value = "${book.id}"/>
				
					<button type = "submit" class = "btn btn-primary">Save</button>
				</form>
			</div>
		</div>
		<a href = "${pageContext.request.contextPath}/BookController?action=LIST">Back to List</a>
	</div>
	
<jsp:include page="../common/footer.jsp"></jsp:include>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>
</html>