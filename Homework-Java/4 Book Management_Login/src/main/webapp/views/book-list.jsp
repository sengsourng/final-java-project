<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>List of Book</title>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<style type="text/css">
	a{
		color:#444 !important;
	}
</style>
</head>
<body>
<%
	if(session.getAttribute("username")==null){
		response.sendRedirect("Login/login.jsp");
	}
%>

	<jsp:include page="../common/header.jsp"></jsp:include>
	

	<div class = "container">
		
		<h1>List All of Books</h1>
		<hr/>
		
		<div>
		  <p>${NOTIFICATION}</p>
		</div>
		
		<p>
			<button class="btn btn-primary" onclick="window.location.href = 'book-form.jsp'">Add New Book</button>
		</p>
	
		<table class="table table-hover table-bordered table-striped">
			
			<tr class="thead-blue">
				<th>ID</th>
				<th>Title</th>
				<th>Author</th>
				<th>Price</th>
				<th>Actions</th>
			</tr>
			
			<c:forEach items="${list}" var="book">
				<tr>
					<td>${book.id}</td>
					<td>${book.title}</td>
					<td>${book.author}</td>
					<td>${book.price}$</td>
					<td> 
						<a href = "${pageContext.request.contextPath}/BookController?action=EDIT&id=${book.id}" class="btn btn-primary badge pill">Edit</a> 
						<a href = "${pageContext.request.contextPath}/BookController?action=DELETE&id=${book.id}" class="btn btn-danger badge pill">Delete</a> 
					</td>
				</tr>
			</c:forEach>
			
		</table>
		
	</div>
<jsp:include page="../common/footer.jsp"></jsp:include>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>
</html>