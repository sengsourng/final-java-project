package sourngtech.com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import sourngtech.com.modal.LoginBean;
import sourngtech.com.util.DBConnectionUtil;

public class LoginDao {
	
	public boolean validate(LoginBean loginBean) throws ClassNotFoundException {
	
		boolean status = false;
		
		String sql="select * from tbluser where username = ? and password = ? ";
		
		Class.forName("com.mysql.cj.jdbc.Driver");

		try {
			
		
			Connection connection = DBConnectionUtil.getConnection();
			// Step 2:Create a statement using connection object
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, loginBean.getUsername());
			preparedStatement.setString(2, loginBean.getPassword());

			System.out.println(preparedStatement);
			ResultSet rs = preparedStatement.executeQuery();
			status = rs.next();

		} catch (SQLException e) {
			// process sql exception
			DBConnectionUtil.printSQLException(e);
		}
		return status;
	}
	
}
