package sourngtech.com.dao;

import java.util.List;

import sourngtech.com.modal.User;

public interface UserDao {
	List<User> get();
	
	User get(int id);
	
	boolean save(User user);
	
	boolean delete(int id);
	
	boolean update(User user);
}
