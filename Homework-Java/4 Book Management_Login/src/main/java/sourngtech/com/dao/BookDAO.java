package sourngtech.com.dao;

import java.util.List;

import sourngtech.com.modal.Book;

public interface BookDAO {
	
	List<Book> get();
	
	Book get(int id);
	
	boolean save(Book book);
	
	boolean delete(int id);
	
	boolean update(Book book);
	
}
