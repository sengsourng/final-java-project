package sourngtech.com.controller;

import java.io.IOException;
import java.util.List;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import sourngtech.com.dao.BookDAO;
import sourngtech.com.dao.BookDAOImpl;
import sourngtech.com.modal.Book;


/**
 * Servlet implementation class BookController
 */
@WebServlet("/BookController")
public class BookController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	RequestDispatcher dispatcher = null;
	BookDAO bookDAO = null;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BookController() {
    	bookDAO = new BookDAOImpl();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String action = request.getParameter("action");
		
		if(action == null) {
			action = "LIST";
		}
		
		switch(action) {
			
			case "LIST":
				listBook(request, response);
				break;
				
			case "EDIT":
				getSingleBook(request, response);
				break;
				
			case "DELETE":
				deleteBook(request, response);
				break;
				
			default:
				listBook(request, response);
				break;
				
		}
	}
	private void deleteBook(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String id = request.getParameter("id");
	
		if(bookDAO.delete(Integer.parseInt(id))) {
			request.setAttribute("NOTIFICATION", "Book Deleted Successfully!");
		}
		
		listBook(request, response);
	}

	private void getSingleBook(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		
		String id = request.getParameter("id");
		
		Book theBook = bookDAO.get(Integer.parseInt(id));
		
		request.setAttribute("book", theBook);
		
		dispatcher = request.getRequestDispatcher("/views/book-form.jsp");
		
		dispatcher.forward(request, response);
	}

	private void listBook(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		List<Book> theList = bookDAO.get();
		
		request.setAttribute("list", theList);
		
		dispatcher = request.getRequestDispatcher("/views/book-list.jsp");
		
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String id = request.getParameter("id");
		
		Book b = new Book();
		b.setTitle(request.getParameter("title"));
		b.setAuthor(request.getParameter("author"));
		b.setPrice(Double.parseDouble(request.getParameter("price")));
		
		if(id.isEmpty() || id == null) {
			
			if(bookDAO.save(b)) {
				request.setAttribute("NOTIFICATION", "Book Saved Successfully!");
			}
		
		}else {
			
			b.setId(Integer.parseInt(id));
			if(bookDAO.update(b)) {
				request.setAttribute("NOTIFICATION", "Book Updated Successfully!");
			}
			
		}
		
		listBook(request, response);
	}

}
