import React, { Component } from 'react'
import UserService from '../services/UserService'

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faUsers } from "@fortawesome/free-solid-svg-icons";
import { faPlus,faTrash,faEdit,faEye,faBan,faUserCheck } from "@fortawesome/free-solid-svg-icons";

library.add(faUsers,faPlus,faTrash,faEdit,faEye,faBan,faUserCheck);

class UserListComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
                users: []
        }

        this.addUser=this.addUser.bind(this);
        this.editUser = this.editUser.bind(this);
        this.deleteUser = this.deleteUser.bind(this);
    
    }

    deleteUser(id){
        UserService.deleteUser(id).then( res => {
            this.setState({users: this.state.users.filter(user => user.id !== id)});
        });
    }
    viewUser(id){
        this.props.history.push(`/view-user/${id}`);
    }
    editUser(id){
        this.props.history.push(`/add-user/${id}`);
    }

    componentDidMount(){
        UserService.getUsers().then((res) => {
            this.setState({ users: res.data});
        });
    }

    addUser(){
        this.props.history.push('/add-user/_add');
    }

    render() {
        return (
            <div>                 
                 <div>
                    <h2 className="text-center khmer-font mt-3">
                    បញ្ជីអ្នកប្រើប្រាស់ទាំងអស់  <button className="btn btn-primary khmer-font float-right" onClick={this.addUser}> <FontAwesomeIcon icon="plus" /> បន្ថែមអ្នកប្រើ</button></h2> 
                   
                        
                 </div>
               
                 <br></br>
                 <div className = "row">
                        <table className = "table table-striped table-bordered">

                            <thead>
                                <tr>
                                    <th className ="khmer-font"> នាមត្រកូល</th>
                                    <th className ="khmer-font"> នាមខ្លួន</th>
                                    <th className ="khmer-font"> អ៊ីម៉ែល</th>
                                    <th className ="khmer-font"> ពាក្យសម្ងាត់</th>
                                    <th className ="khmer-font"> អាស័យដ្ឋាន</th>
                                    <th className ="khmer-font"> សកម្មភាព</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.users.map(
                                        user => 
                                        <tr key = {user.id}>
                                             <td className ="khmer-font"> { user.firstName} </td>   
                                             <td className ="khmer-font"> {user.lastName}</td>
                                             <td className ="khmer-font"> {user.emailId}</td>
                                             <td className ="khmer-font">
                                                   
                                              {user.status==0?<button className="btn btn-danger btn-sm khmer-font"><FontAwesomeIcon icon="ban" /> មិនអនុញ្ញាត </button>:<button className="btn btn-success btn-sm khmer-font"><FontAwesomeIcon icon="user-check" />  អនុញ្ញាត </button>}
                                              </td>
                                             <td className ="khmer-font"> {user.address}</td>
                                             <td>
                                                 <button onClick={ () => this.editUser(user.id)} className="btn btn-info btn-sm khmer-font"><FontAwesomeIcon icon="edit" />  កែប្រែ </button>
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.deleteUser(user.id)} className="btn btn-danger btn-sm khmer-font"><FontAwesomeIcon icon="trash" /> លុប </button>
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.viewUser(user.id)} className="btn btn-info btn-sm khmer-font"><FontAwesomeIcon icon="eye" />  បង្ហាញ </button>
                                             </td>
                                        </tr>
                                    )
                                }
                            </tbody>
                        </table>

                 </div>

            </div>
        )
    }
}

export default UserListComponent
