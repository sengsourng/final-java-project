import React, { Component } from 'react'
import EmployeeService from '../services/EmployeeService'

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faUsers } from "@fortawesome/free-solid-svg-icons";
import { faPlus,faTrash,faEdit,faEye } from "@fortawesome/free-solid-svg-icons";

library.add(faUsers,faPlus,faTrash,faEdit,faEye);

class ListEmployeeComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
                employees: []
        }
        this.addEmployee = this.addEmployee.bind(this);
        this.editEmployee = this.editEmployee.bind(this);
        this.deleteEmployee = this.deleteEmployee.bind(this);
    }

    deleteEmployee(id){
        EmployeeService.deleteEmployee(id).then( res => {
            this.setState({employees: this.state.employees.filter(employee => employee.id !== id)});
        });
    }
    viewEmployee(id){
        this.props.history.push(`/view-employee/${id}`);
    }
    editEmployee(id){
        this.props.history.push(`/add-employee/${id}`);
    }

    componentDidMount(){
        EmployeeService.getEmployees().then((res) => {
            this.setState({ employees: res.data});
        });
    }

    addEmployee(){
        this.props.history.push('/add-employee/_add');
    }

    render() {
        return (
            <div>                 
                 <div>
                    <h2 className="text-center khmer-font mt-3">
                    បញ្ជីអតិថិជនទាំងអស់  <button className="btn btn-primary khmer-font float-right" onClick={this.addEmployee}> <FontAwesomeIcon icon="plus" /> បន្ថែមអតិថិជនថ្មី</button></h2> 
                   
                        
                 </div>
               
                 <br></br>
                 <div className = "row">
                        <table className = "table table-striped table-bordered">

                            <thead>
                                <tr>
                                    <th className ="khmer-font"> នាមត្រកូល</th>
                                    <th className ="khmer-font"> នាមខ្លួន</th>
                                    <th className ="khmer-font"> អ៊ីម៉ែល</th>
                                    <th className ="khmer-font"> ទូរស័ព្ទ</th>
                                    <th className ="khmer-font"> អាស័យដ្ឋាន</th>
                                    <th className ="khmer-font"> សកម្មភាព</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.employees.map(
                                        employee => 
                                        <tr key = {employee.id}>
                                             <td className ="khmer-font"> { employee.firstName} </td>   
                                             <td className ="khmer-font"> {employee.lastName}</td>
                                             <td className ="khmer-font"> {employee.emailId}</td>
                                             <td className ="khmer-font"> {employee.phone}</td>
                                             <td className ="khmer-font"> {employee.address}</td>
                                             <td>
                                                 <button onClick={ () => this.editEmployee(employee.id)} className="btn btn-info btn-sm khmer-font"><FontAwesomeIcon icon="edit" />  កែប្រែ </button>
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.deleteEmployee(employee.id)} className="btn btn-danger btn-sm khmer-font"><FontAwesomeIcon icon="trash" /> លុប </button>
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.viewEmployee(employee.id)} className="btn btn-info btn-sm khmer-font"><FontAwesomeIcon icon="eye" />  បង្ហាញ </button>
                                             </td>
                                        </tr>
                                    )
                                }
                            </tbody>
                        </table>

                 </div>

            </div>
        )
    }
}

export default ListEmployeeComponent
