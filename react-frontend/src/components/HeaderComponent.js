import React, { Component } from 'react'

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faUsers } from "@fortawesome/free-solid-svg-icons";
import { faPlus } from "@fortawesome/free-solid-svg-icons";

library.add(faUsers,faPlus);


class HeaderComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
                 
        }
    }

    render() {
        return (
            <div>
                <header>
                   
                     <nav className="navbar navbar-expand-md navbar-dark bg-dark">
                        <div className="container-fluid">
                            <div className="navbar-header">
                            <a className="navbar-brand" href="#"><span className="font-khmer"> <FontAwesomeIcon icon="users" /> ប្រព័ន្ធគ្រប់គ្រងពត៌មានអតិថិជន </span></a>
                            </div>
                            <ul className="nav navbar-nav">
                                <li className="active"><a href="/" className="mr-4 khmer-font">ទំព័រដើម</a></li>
                                <li><a href="/employees" className="mr-4 khmer-font" >អតិថិជន</a></li>
                                <li><a href="/users" className="mr-4 khmer-font" >អ្នកប្រើប្រាស់</a></li>
                            </ul>
                            <ul className="nav navbar-nav navbar-right">
                                {/* <button className="btn btn-primary khmer-font float-right" onClick={this.addEmployee}> <FontAwesomeIcon icon="plus" /> បន្ថែមអតិថិជនថ្មី</button> */}
                                <li><a href="#" className="btn btn-success khmer-font mr-2"><span className="glyphicon glyphicon-user"></span> ចុះឈ្មោះ</a></li>
                                <li><a href="#" className="btn btn-info khmer-font"><span className="glyphicon glyphicon-log-in"></span> ចូលប្រើ</a></li>
                            </ul>
                        </div>
                    </nav>


                    
                </header>

               
            </div>
        )
    }
}

export default HeaderComponent
