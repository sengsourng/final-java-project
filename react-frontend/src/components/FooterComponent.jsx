import React, { Component } from 'react'

class FooterComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
                 
        }
    }

    render() {
        return (
            <div className="mt-5">
                <footer className = "footer mt-15">
                    <div className="pt-3">
                        <span className="text-muted pt-2">All Rights Reserved 2021 @ SENG Sourng - MIT@BBU</span>
                    </div>
                </footer>
            </div>
        )
    }
}

export default FooterComponent
